## Insert your Dockerfile directives here to build the proper Docker image
FROM ubuntu:latestubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python-pip python-dev build-essential
COPY . /app
WOEKDIR /app
RUN pip install -r requirements.txt
EntryPOINT["python", "app.py"]